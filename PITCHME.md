# Rails Application Templates

---

## how to use

```
rails new test_app -m ~/work/rp/template_example/template.rb
```
or

```
rails new test_app --template https://gitlab.com/Pomeha/rails-application-teamplate-example/-/raw/master/template.rb
```

---

# Create your own template

---

#### Adding new gems

```ruby
gem 'sidekiq', '~> 6.0', '>= 6.0.6'
gem_group :development, :test do
  gem 'rubocop'
  gem 'pry-byebug'
end
```

---

#### The flow based on the user's answer + adding new files

```ruby
if yes?('add dry auto_inject?')
  gem 'dry-auto_inject'

  initializer 'auto_inject.rb', <<-CODE
    # frozen_string_literal: true

    class DryContainer
      extend Dry::Container::Mixin
    end

    DI = Dry::AutoInject(DryContainer)
  CODE
end
```

_output:_

```sh
  dry auto_inject? yes
      gemfile    dry-auto_inject
  initializer    auto_inject.rb
```

---

```ruby

if yes?('services and queries?')
  file 'app/services/.keep'
  file 'app/queries/.keep'
end
```

---

#### scaffold? really?

```ruby
generate(:scaffold, 'book', 'title:string', 'description:text', 'pages:number')
route "root to: 'books#index'"
```

---

#### Adding gems does not install them!

Just add this command:

```ruby
run 'bundle install'
```

---

#### after_bundle

```ruby
after_bundle do
  run 'rspec --init'
  run 'bundle exec rubocop -A'

  git :init
  git add: '.'
  git commit: "-m 'Initial commit'"
end
```

---

# Thank you!
