gem 'fast_jsonapi', '~> 1.5'
gem 'oj', '~> 3.7.10'
gem 'devise'
gem 'sidekiq', '~> 5.2.5'
gem 'rollbar', '~> 2.19.2'
gem 'dotenv-rails'
gem 'apitome'

gem_group :development, :test do
  gem 'rubocop'
  gem 'pry-byebug'
  gem 'pry-doc'
  gem 'pry-rails'
  gem 'rspec-rails', '~> 3.8'
  gem 'faker', '~> 1.9.1'
  gem 'factory_bot_rails', '~> 4.11.1'
  gem 'rspec_api_documentation'
end

file '.rubocop.yml', <<-CODE
  require:

  AllCops:
    NewCops: enable
    Exclude:
      - db/**
      - db/migrate/**
      - bin/**
      - vendor/**/*

  Layout/LineLength:
    Max: 120

  Metrics/BlockLength:
    Exclude:
      - config/**/*

  Style/Documentation:
    Enabled: false
CODE

if yes?('dry auto_inject?')
  gem 'dry-auto_inject'

  initializer 'auto_inject.rb', <<-CODE
    # frozen_string_literal: true

    class DryContainer
      extend Dry::Container::Mixin
    end

    DI = Dry::AutoInject(DryContainer)
  CODE
end

gem 'dry-validation', '0.13.0' if yes?('dry validations?')

if yes?('services and queries?')
  file 'app/services/.keep'
  file 'app/queries/.keep'
end

run 'bundle install'

after_bundle do
  run 'rspec --init'
  run 'bundle exec rubocop -A'

  git :init
  git add: '.'
  git commit: "-m 'Initial commit'"
end
